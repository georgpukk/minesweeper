function intArray(length) {
    var a = [];
    var i = 0;
    while(a.push(i++)<length){};
    return a
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
    return a
}

function areConnected(id1, id2) {
    if (Math.abs(id1-id2) == 1 && Math.floor(id1 / sideLength) == Math.floor(id2 / sideLength))
        return true;
    else if (Math.abs(id1-id2) > 1 && Math.abs(Math.floor(id1 / sideLength) - Math.floor(id2 / sideLength)) == 1)
        return true;
    return false;
}

function surroundingTiles(id) {
    var initialTiles = [id-sideLength-1, id-sideLength, id-sideLength+1, id-1, id+1, id+sideLength-1, id+sideLength, id+sideLength+1];
    var surrTiles = [];
    for (var i = 0; i < initialTiles.length; i++) {
        if (areConnected(id, initialTiles[i]) && initialTiles[i] >= 0 && initialTiles[i] < amountOfTiles)
            surrTiles.push(initialTiles[i]);
    }
    return surrTiles;
}

function createTilesArray(length) {
    array = [];
    for (var i = 0; i < length; i++) {
        tile = {};
        tile.id = i;
        tile.hasBomb = false;
        tile.surroundingBombs = 0;
        tile.visited = false;
        tile.hasFlag = false;
        array.push(tile);
    }
    return array;
}

function bombClicked(id) {
    for (var i = 0; i < amountOfBombs; i++) {
        var tile = tiles[bombs[i]];
        var dom = document.getElementById(tile.id);
        if (dom.innerHTML.length == 0) {
            dom.innerHTML += '<div class="bomb"></div>';
            dom.classList.add('visited');
            if (tile.id == id)
                dom.style.backgroundImage = 'linear-gradient(135deg, #E00, #C00)';
        }
    }
}

function manageFlag(id) {
    var tile = tiles[id];
    var dom = document.getElementById(tile.id);
    if (tile.hasFlag) {
        dom.innerHTML = '';
        flagCounter++;
        document.getElementById('flags').innerHTML = flagCounter;
        tile.hasFlag = false;
        if (tile.visited && tile.surroundingBombs > 0 && dom.innerHTML.length == 0) {
            dom.classList.add('visited');
            dom.style.color = colors[tile.surroundingBombs];
            dom.innerHTML = tile.surroundingBombs;
        }
    } else if (!tile.visited && dom.innerHTML.length == 0 && flagCounter > 0) {
        flagCounter--;
        dom.innerHTML = '<div class="flag"></div>';
        tile.hasFlag = true;
        document.getElementById('flags').innerHTML = flagCounter;
    }
}

function dfs(id) {
    while (stack.length > 0) {
        var surrTiles = surroundingTiles(id);
        for (var i = 0; i < surrTiles.length; i++) {
            var current = tiles[surrTiles[i]];
            if (!current.visited && !current.hasBomb) {
                current.visited = true;
                if (unvisited.indexOf(current.id) > -1)
                    unvisited.splice(unvisited.indexOf(current.id), 1);
                result.push(current.id);
                if (current.surroundingBombs == 0) {
                    stack.push(current.id);
                    dfs(current.id);
                }
            }
        }
        stack.pop();
    }
    for (var i = 0; i < result.length; i++) {
        temp = tiles[result[i]];
        dom = document.getElementById(temp.id);
        if (temp.surroundingBombs > 0) {
            if (dom.innerHTML.length == 0 && !temp.hasBomb) {
                dom.innerHTML += temp.surroundingBombs;
                dom.classList.add('visited');
                dom.style.color = colors[temp.surroundingBombs];
            }
        } else {
            dom.classList.add('visited');
        }
    }
}

function resetGlobalVariables() {
    flagCounter = amountOfBombs;
    document.getElementById('flags').innerHTML = flagCounter;
    unvisited = [];
    start = false;
    tiles = createTilesArray(amountOfTiles);
}

function resetDom() {
    document.getElementById('menu').style.display = 'none';
    for (var i = 0; i < tileNodes.length; i++) {
        tileNodes[i].classList.remove('visited');
        tileNodes[i].innerHTML = '';
        tileNodes[i].removeAttribute('style');
        tileNodes[i].addEventListener('click', myClick);
        tileNodes[i].addEventListener('contextmenu', myRightClick);
    }
    clearInterval(updateTimer);
    document.getElementById('time').innerHTML = 0;
}

function removeEventListeners() {
    for (var i = 0; i < tileNodes.length; i++) {
        tileNodes[i].removeEventListener('click', myClick);
        tileNodes[i].removeEventListener('contextmenu', myRightClick);
    }
}

function myClick() {
    tile = tiles[this.id];
    if (!tile.hasFlag) {
        if (!start) {
            startTime = new Date();
            updateTimer = setInterval(function() {
                document.getElementById('time').innerHTML = Math.floor((new Date().getTime() - startTime.getTime()) / 1000);
            }, 1000);
            
            start = true;
            // creates bombs array
            allTiles = intArray(amountOfTiles);
            surTiles = surroundingTiles(tile.id);
            allTiles.splice(allTiles.indexOf(tile.id), 1);
            for (var i = 0; i < surTiles.length; i++) {
                allTiles.splice(allTiles.indexOf(surTiles[i]), 1);
            }
            bombs = shuffle(allTiles).slice(0, amountOfBombs);
            // places bombs and gets tile.surroundingBombs
            for (var i = 0; i < amountOfBombs; i++) {
                curr = tiles[bombs[i]];
                if (curr) {
                    curr.hasBomb = true;
                    surrTiles = surroundingTiles(curr.id);
                    for (var j = 0; j < surrTiles.length; j++) {
                        if (tiles[surrTiles[j]])
                            tiles[surrTiles[j]].surroundingBombs++; 
                    }
                }
            }
            //creates unvisited array
            for (var i = 0; i < amountOfTiles; i++) {
                if (!tiles[i].hasBomb) {
                    unvisited.push(i);
                }
            }
            document.getElementById('reset').onclick = function() {
                resetGlobalVariables();
                resetDom();
            }
            document.addEventListener('keyup', function (e) {
                var key = e.which || e.keyCode;
                if (key == 82 && start) {
                    resetGlobalVariables();
                    resetDom();
                }
            });
        }
        if (tile.hasBomb) {
            bombClicked(tile.id);
            clearInterval(updateTimer);
            document.getElementById('time').innerHTML = (new Date().getTime() - startTime.getTime()) / 1000;
            removeEventListeners();
        } else if (tile.visited) {
            if (tile.surroundingBombs > 0) {
                var amountOfFlags = 0;
                var amountOfRightFlags = 0;
                var redBombs = [];
                surroundingTiles(tile.id).forEach(function(surr) {
                    if (tiles[surr].hasFlag) {
                        amountOfFlags++;
                        if (tiles[surr].hasBomb)
                            amountOfRightFlags++;
                    } else if (tiles[surr].hasBomb) {
                        redBombs.push(surr);
                    }
                });
                if (amountOfFlags == tile.surroundingBombs) {
                    result = [tile.id];
                    stack = [tile.id];
                    dfs(tile.id);
                    if (amountOfRightFlags != amountOfFlags) {
                        clearInterval(updateTimer);
                        document.getElementById('time').innerHTML = (new Date().getTime() - startTime.getTime()) / 1000;
                        bombClicked(tile.id);
                        for (var i = 0; i < redBombs.length; i++) {
                            document.getElementById(redBombs[i]).style.backgroundImage = 'linear-gradient(135deg, #E00, #C00)';
                        }
                        removeEventListeners();
                    }
                }
            }
        } else {
            tile.visited = true;
            if (unvisited.indexOf(tile.id) > -1)
                unvisited.splice(unvisited.indexOf(tile.id), 1);
            if (tile.surroundingBombs == 0) {
                result = [tile.id];
                stack = [tile.id];
                dfs(tile.id);
            } else {
                var dom = document.getElementById(tile.id);
                dom.innerHTML += tile.surroundingBombs;
                dom.classList.add('visited');
                dom.style.color = colors[tile.surroundingBombs];
            }
        }
    }
    if (start && unvisited.length < 1) {
        clearInterval(updateTimer);
        document.getElementById('time').innerHTML = (new Date().getTime() - startTime.getTime()) / 1000;
        removeEventListeners();
        document.getElementById('menu').style.display = 'block';
    }
}

function myRightClick(e) {
    e.preventDefault();
    manageFlag(this.id);
    previousID = this.id;
}

window.onload = function() {

    colors = ['#000', '#00F', '#007B00', '#F00', '#00007B', '#7B0000', '#007B7B', '#000', '#7B7B7B'];
    sideLength = 16;
    calcLength = sideLength + 2;
    amountOfTiles = sideLength * sideLength;
    amountOfBombs = 40;

    flagCounter = amountOfBombs;
    document.getElementById('flags').innerHTML = flagCounter;
    unvisited = [];
    start = false;
    tiles = createTilesArray(amountOfTiles);

    tileWidth = 100 / calcLength;
    tileFontSize = 81 / calcLength;
    bombWidth = 72 / calcLength;
    bombBorderRadius = bombWidth / 2;
    bombMargin = 14 / calcLength;
    poleWidth = 5 / calcLength;
    poleHeight = 54 / calcLength;
    flagWidth = 45 / calcLength;
    flagHeight = 0.7 * poleHeight;
    flagMargin = 23 / calcLength;

    document.head.innerHTML += '<style type="text/css">\
        #container {\
            width: '+ 100 +'vmin;\
            height: '+ 100 +'vmin;\
            padding: '+ tileWidth +'vmin;\
        }\
        #menu {\
            top: '+ tileWidth +'vmin;\
            right: '+ tileWidth +'vmin;\
            bottom: '+ tileWidth +'vmin;\
            left: '+ tileWidth +'vmin;\
        }\
        #time {\
            line-height: '+ tileWidth +'vmin;\
            font-size: '+ tileFontSize +'vmin;\
        }\
        #flags {\
            line-height: '+ tileWidth +'vmin;\
            font-size: '+ tileFontSize +'vmin;\
            right: '+ tileWidth +'vmin;\
        }\
        #reset {\
            line-height: '+ tileWidth +'vmin;\
            font-size: '+ tileFontSize +'vmin;\
            left: '+ 40 +'%;\
        }\
        .tile {\
            width: '+tileWidth+'vmin;\
            height: '+tileWidth+'vmin;\
            line-height: '+tileWidth+'vmin;\
            font-size: '+tileFontSize+'vmin;\
        }\
        .bomb {\
            width: '+bombWidth+'vmin;\
            height: '+bombWidth+'vmin;\
            border-radius: '+bombBorderRadius+'vmin;\
            margin: '+bombMargin+'vmin auto;\
        }\
        .flag {\
            width: '+flagWidth+'vmin;\
            height: '+flagHeight+'vmin;\
            margin: '+flagMargin+'vmin auto;\
        }\
        .flag:after {\
            border-width: '+poleWidth+'vmin;\
            height: '+poleHeight+'vmin;\
        }\
        .visited {\
            background-image: linear-gradient(135deg, #CCC, #BBB);\
        }\
    </style>';

    container = document.getElementById('container');

    tiles.forEach(function(tile) {
        container.innerHTML += '<div class="tile" id="'+tile.id+'"></div>';
    });
    tileNodes = document.getElementsByClassName('tile');

    for (var j = 0; j < tileNodes.length; j++) {
        tileNodes[j].addEventListener('click', myClick);
    }

    for (var j = 0; j < tileNodes.length; j++) {
        tileNodes[j].addEventListener('contextmenu', myRightClick);
    }
    
}
